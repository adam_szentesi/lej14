public enum TileType
{
  Error,
  Empty,
  Sponge,
  Acid,
  Diamondium,
  Bismol,
  Jam,
  Weevil,
  Latinum,
}