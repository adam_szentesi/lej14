using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class TileGrid : MonoBehaviour
{
  public int Width = 6;
  public int Height = 20;
  public GameObject TileSlotPrefab;
  public PixelPerfectCamera PixelPerfectCamera;
  public TileBank TileBank;
  public Image NextImage;

  public AudioSource PopAudio;
  public AudioSource PushAudio;

  private bool IsReadyToSpawn = true;
  private TileSlot[,] TileSlots;
  private List<Tile> ControllableTiles = new List<Tile>();

  public static TileGrid Instance;

  private GameObject TilePrefab => TileBank.TilePrefab;
  private int GetTileScore(TileType tileType) => TileBank.GetTileScore(tileType);
  private Sprite GetTileSprite(TileType tileType) => TileBank.GetTileSprite(tileType);

  private TileType NextSpawnTileType;
  private Vector2Int SpawnTilePosition;
  private Action OnBakeOver;

  public int LayerScore
  {
    get
    {
      int score = 0;

      for (int y = 0; y < Height; y++)
      {
        TileType tileType = GetTileType(0, y);

        if (tileType == TileType.Empty) continue;

        bool isAllSame = true;

        for (int x = 1; x < Width; x++)
        {
          TileType otherTileType = GetTileType(x, y);

          if (tileType != otherTileType)
          {
            isAllSame = false;
            break;
          }
        }

        if (isAllSame) score += GetTileScore(tileType) * Width * 2;

      }

      return score;
    }
  }

  private void Awake()
  {
    if (Instance)
    {
      Destroy(this);
      return;
    }

    Instance = this;
  }

  public void Init(Action onBakeOver, int width, int height)
  {
    Width = width;
    Height = height;

    TileSlots = new TileSlot[Width, Height + 1];

    TileBank.Init();

    SpawnTilePosition = new Vector2Int(Width / 2, Height);

    for (int y = 0; y < Height; y++)
    {
      for (int x = 0; x < Width; x++)
      {
        SpawnSlot(x, y);
      }
    }

    SpawnSlot(SpawnTilePosition.x, SpawnTilePosition.y);

    float cameraX = Width / 2.0f - 0.5f;
    float cameraY = 13.0f;

    PixelPerfectCamera.transform.position = new Vector3(cameraX, cameraY, -10.0f);
    MakeNextSpawnTileType();

    OnBakeOver = onBakeOver;
  }

  private void SpawnSlot(int x, int y)
  {
    GameObject tileSlotGO = Instantiate(TileSlotPrefab, transform);
    TileSlot tileSlot = tileSlotGO.GetComponent<TileSlot>();
    tileSlot.Init(new Vector2Int(x, y));
    TileSlots[x, y] = tileSlot;
  }

  private void MakeNextSpawnTileType()
  {
    NextSpawnTileType = TileBank.GetSpawnableTileType(UnityEngine.Random.Range(0, TileBank.SpawnableTileTypeCount));
    NextImage.sprite = GetTileSprite(NextSpawnTileType);
  }

  private void SpawnTile()
  {
    AddTile(SpawnTilePosition.x, SpawnTilePosition.y, NextSpawnTileType);
    MakeNextSpawnTileType();

    TileSlot currentTileSlot = GetTileSlotUnsafe(SpawnTilePosition);
    if (!currentTileSlot.CanFall) OnBakeOver?.Invoke();

    //DEBUG
    //Drop();

    IsReadyToSpawn = false;
  }

  private bool AddTile(int x, int y, TileType tileType)
  {
    TileSlot tileSlot = TileSlots[x, y];
    TileBehaviour tileBehaviour = TileBank.GetTileBehaviour(tileType);

    if (!tileBehaviour)
    {
      Debug.LogError("TileGrid.AddTile: TileBehaviour for " + tileType + " missing!");
      return false;
    }

    GameObject tileGO = Instantiate(TilePrefab, transform);
    Tile tile = tileGO.GetComponent<Tile>();
    tile.Init(tileBehaviour, UnregisterControllableTile);
    tileSlot.SetTile(tile);
    RegisterControllableTile(tile);

    return true;
  }

  private void RegisterControllableTile(Tile tile)
  {
    if (!tile || ControllableTiles.Contains(tile)) return;

    ControllableTiles.Add(tile);
  }

  private void UnregisterControllableTile(Tile tile)
  {
    if (!tile || !ControllableTiles.Contains(tile)) return;
    ControllableTiles.Remove(tile);
    //Debug.Log("UnregisterFallingTile " + tile.TilePosition + " / " + ControllableTiles.Count);

    if (ControllableTiles.Count == 0) IsReadyToSpawn = true;
  }

  public Tile GetTile(Vector2Int tilePosition)
  {
    TileSlot tileSlot = GetTileSlot(tilePosition);
    if (!tileSlot) return null;
    return tileSlot.Tile;
  }

  public TileType GetTileType(Vector2Int tilePosition)
  {
    return GetTileType(tilePosition.x, tilePosition.y);
  }

  public TileType GetTileType(int x, int y)
  {
    TileSlot tileSlot = GetTileSlot(x, y);
    if (!tileSlot) return TileType.Error;
    return tileSlot.TileType;
  }

  private TileSlot GetTileSlot(Vector2Int tilePosition)
  {
    return GetTileSlot(tilePosition.x, tilePosition.y);
  }

  private TileSlot GetTileSlot(int x, int y)
  {
    if (!IsWithinGrid(x, y)) return null;

    return GetTileSlotUnsafe(x, y);
  }

  private TileSlot GetTileSlotUnsafe(Vector2Int tilePosition)
  {
    return GetTileSlotUnsafe(tilePosition.x, tilePosition.y);
  }

  private TileSlot GetTileSlotUnsafe(int x, int y)
  {
    return TileSlots[x, y];
  }

  private bool IsWithinGrid(int x, int y)
  {
    if (x < 0 || x >= Width || y < 0 || y >= Height) return false;
    return true;
  }

  public void SpawnTiles()
  {
    if (IsReadyToSpawn) SpawnTile();
  }

  public void LowerTiles()
  {
    for (int y = 0; y < Height; y++)
    {
      for (int x = 0; x < Width; x++)
      {
        LowerTile(new Vector2Int(x, y));
      }
    }

    LowerTile(SpawnTilePosition);
  }

  private bool LowerTile(Vector2Int currentPosition)
  {
    return LowerTile(currentPosition, currentPosition + new Vector2Int(0, -1));
  }

  private bool LowerTile(Vector2Int currentPosition, Vector2Int targetPosition)
  {
    TileSlot currentTileSlot = GetTileSlotUnsafe(currentPosition);

    if (!currentTileSlot.CanFall) return false;

    TileSlot targetTileSlot = GetTileSlotUnsafe(targetPosition);

    targetTileSlot.SetTileToSwap(currentTileSlot.GetTile());
    currentTileSlot.ClearTile();
    targetTileSlot.SwapTiles();

    return true;
  }

  public int GroundTiles()
  {
    int score = 0;

    for (int y = 0; y < Height; y++)
    {
      for (int x = 0; x < Width; x++)
      {
        TileSlot tileSlot = GetTileSlot(x, y);

        if (!tileSlot.Tile) continue;
        
        score += GroundTile(tileSlot.Tile);
      }
    }

    return score;
  }

  private int GroundTile(Tile tile)
  {
    int score = 0;

    if (!tile.CanFall && tile.IsControllable)
    {
      UnregisterControllableTile(tile);
      tile.DisableControls();
      score = GetTileScore(tile.TileType);
    }

    if(score > 0 && !PushAudio.isPlaying)
    {
      PushAudio.Play();
    }

    return score;
  }

  public void ToggleTiles()
  {
    for (int y = 0; y < Height; y++)
    {
      for (int x = 0; x < Width; x++)
      {
        GetTileSlot(x, y).Toggle();
      }
    }
  }

  public int DestroyTiles()
  {
    int score = 0;

    for (int y = 0; y < Height; y++)
    {
      for (int x = 0; x < Width; x++)
      {
        TileSlot tileSlot = GetTileSlot(x, y);

        if (!tileSlot.IsToBeDestroyed) continue;

        score += GetTileScore(tileSlot.TileType);
        tileSlot.DestroyTile();
      }
    }

    if (score > 0 && !PopAudio.isPlaying)
    {
      PopAudio.Play();
    }

    return score;
  }

  public void ReconnectTiles()
  {
    for (int y = 0; y < Height; y++)
    {
      for (int x = 0; x < Width; x++)
      {
        GetTileSlot(x, y).Reconnect();
      }
    }
  }

  public void Move(int value)
  {
    Move(new Vector2Int(value, 0));
  }

  public void Move(Vector2Int offset)
  {
    foreach (Tile tile in ControllableTiles)
    {
      if (tile.TilePosition == SpawnTilePosition) return;

      Vector2Int targetPosition = tile.TilePosition + offset;

      TileSlot targetTileSlot = GetTileSlot(targetPosition);
      
      if (!targetTileSlot) return;

      TileType targetTileType = targetTileSlot.TileType;

      if (!targetTileSlot.IsControllable && !tile.CanOverlap(targetTileType)) return;
    }

    TileSlot[] tileSlotsToSwap = new TileSlot[ControllableTiles.Count];

    for(int i = 0; i < ControllableTiles.Count; i++)
    {
      Tile tile = ControllableTiles[i];
      Vector2Int targetPosition = tile.TilePosition + offset;
      TileSlot targetTileSlot = GetTileSlot(targetPosition);
      TileSlot currentTileSlot = GetTileSlot(tile.TilePosition);

      targetTileSlot.SetTileToSwap(tile);
      currentTileSlot.ClearTile();

      tileSlotsToSwap[i] = targetTileSlot;
    }

    foreach (TileSlot tileSlot in tileSlotsToSwap)
    {
      tileSlot.SwapTiles();
    }

  }

  public void Lower()
  {
    Move(new Vector2Int(0, -1));
  }

  public int Drop()
  {
    List<Tile> tilesToGround = new List<Tile>();
    int score = 0;

    foreach (Tile tile in ControllableTiles)
    {
      if (tile.TilePosition == SpawnTilePosition) continue;

      int yUnder = tile.TilePosition.y - 1;
      int x = tile.TilePosition.x;

      if (yUnder <= 0) continue;

      Vector2Int targetPosition = tile.TilePosition;

      for (int y = yUnder; y >= 0; y--)
      {
        if (GetTileType(x, y) == TileType.Empty)
        {
          targetPosition.y = y;
        }
        else
        {
          break;  
        }
      }

      LowerTile(tile.TilePosition, targetPosition);
      tilesToGround.Add(tile);
    }

    foreach (Tile tile in tilesToGround)
    {
      score += GroundTile(tile);
    }

    return score;
  }

  public bool MarkForDestruction(Vector2Int targetPosition)
  {
    TileSlot tileSlot = GetTileSlot(targetPosition);
    if (tileSlot.IsControllable) return false;
    return tileSlot.MarkForDestruction();
  }

  public bool IsToBeDestroyed(Vector2Int targetPosition)
  { 
    return GetTileSlot(targetPosition).IsToBeDestroyed;
  }

}
