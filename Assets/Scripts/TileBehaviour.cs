using UnityEngine;

public abstract class TileBehaviour : ScriptableObject
{
  public TileData TileData;

  public TileType TileType;

  public Vector2Int TilePosition { get; private set; }

  public static Vector2Int[] NeighborOffsets = new Vector2Int[]
  {
    new Vector2Int(-1, 0),
    new Vector2Int(1, 0),
    new Vector2Int(0, -1),
    new Vector2Int(0, 1),
  };

  public void SetTilePosition(Vector2Int tilePosition)
  {
    TilePosition = tilePosition;
  }

  public abstract void Toggle();
}
