using UnityEngine;

public class TileSlot : MonoBehaviour
{
  public Tile Tile;

  public TileType TileType => Tile ? Tile.TileType : TileType.Empty;
  public bool IsControllable => Tile ? Tile.IsControllable : false;
  public Vector2Int TileSlotPosition { get; private set; }
  public bool IsToBeDestroyed => Tile ? Tile.IsToBeDestroyed : false;

  private Tile TileToSwap;

  public void Init(Vector2Int slotTilePosition)
  {
    TileSlotPosition = slotTilePosition;
    transform.position = new Vector3(TileSlotPosition.x, TileSlotPosition.y, 1);
    name = "Slot[" + TileSlotPosition.x + ";" + TileSlotPosition.y + "]";
  }

  public void SetTile(Tile tileToSwap)
  {
    SetTileToSwap(tileToSwap);
    SwapTiles();
  }

  public void SwapTiles()
  {
    Tile = TileToSwap;
    if (Tile) Tile.SetTilePosition(TileSlotPosition);
  }

  public void SetTileToSwap(Tile tileToSwap)
  {
    TileToSwap = tileToSwap;
  }

  public void ClearTile()
  {
    Tile = null;
  }

  public Tile GetTile() => Tile;

  public bool CanFall => Tile ? Tile.CanFall : false;
  public void Toggle() => Tile?.Toggle();

  public void DisableControls()
  {
    Tile?.DisableControls();
  }

  public bool MarkForDestruction()
  {
    return Tile ? Tile.MarkForDestruction() : false;
  }

  public void DestroyTile()
  {
    Tile?.Selfdestruct();
    ClearTile();
  }

  public void Reconnect()
  {
    Tile?.Reconnect();
  }

}
