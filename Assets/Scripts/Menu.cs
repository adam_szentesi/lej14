using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
  public TileBank TileBank;
  public GameObject CookbookItemPrefab;
  public VerticalLayoutGroup CookbookItems;

  public Panel MainPanel;
  public Panel CookbookPanel;
  public Panel TutorialPanel;

  public Image IngedientIcon;
  public Text IngedientNameText;
  public Text IngedientDescriptionText;

  public Button BakeButton;
  public Button CookbookButton;
  public Button ExitButton;

  public Button CookbookBackButton;
  public Button TutorialBackButton;
  public Button LetsBakeButton;

  public IntNumber WidthNumber;
  public IntNumber HeightNumber;
  public IntNumber SpeedNumber;

  public static int Width;
  public static int Height;
  public static int Speed;

  private Panel CurrentPanel;
  private Button[] IngredientButtons;

  private void Awake()
  {
    TileBank.Init();

    BakeButton.onClick.AddListener(Bake);
    CookbookButton.onClick.AddListener(Cookbook);
    ExitButton.onClick.AddListener(Exit);
    TutorialBackButton.onClick.AddListener(() => SetCurrentPanel(MainPanel));
    CookbookBackButton.onClick.AddListener(() => SetCurrentPanel(MainPanel));
    LetsBakeButton.onClick.AddListener(LetsBake);

    IngredientButtons = new Button[TileBank.SpawnableTileTypeCount];

    for(int i = 0; i < TileBank.SpawnableTileTypeCount; i++)
    {
      TileType tileType = TileBank.GetSpawnableTileType(i);
      TileData tileData = TileBank.GetTileData(tileType);
      GameObject buttonGO = Instantiate(CookbookItemPrefab, CookbookItems.transform);
      buttonGO.GetComponentInChildren<Text>().text = tileData.TileName;
      IngredientButtons[i] = buttonGO.GetComponent<Button>();
      IngredientButtons[i].onClick.AddListener(() => PopulateIngredient(tileType));
    }

    PopulateIngredient(TileType.Sponge);
    SetCurrentPanel(MainPanel);

    WidthNumber.Init(3, 25, 2, 7);
    HeightNumber.Init(3, 25, 1, 13);
    SpeedNumber.Init(1, 10, 1, 5);
  }

  private void PopulateIngredient(TileType tileType)
  {
    TileData tileData = TileBank.GetTileData(tileType);

    IngedientIcon.sprite = tileData.TileSprite;
    IngedientNameText.text = tileData.TileName + " - �" + tileData.TileScore.ToString();
    IngedientDescriptionText.text = tileData.TileDescription;
  }

  private void SetCurrentPanel(Panel panel)
  {
    CurrentPanel?.Toggle(false);
    CurrentPanel = panel;
    CurrentPanel?.Toggle(true);
  }

  private void Bake()
  {
    SetCurrentPanel(TutorialPanel);
  }

  private void LetsBake()
  {
    Width = WidthNumber.Value;
    Height = HeightNumber.Value;
    Speed = SpeedNumber.Value;

    SceneManager.LoadScene(1);
  }

  private void Cookbook()
  {
    SetCurrentPanel(CookbookPanel);
  }

  private void Exit()
  {
    Application.Quit();
  }

  private void OnDestroy()
  {
    foreach (Button button in IngredientButtons)
    {
      button.onClick.RemoveAllListeners();
    }

    BakeButton.onClick.RemoveAllListeners();
    CookbookButton.onClick.RemoveAllListeners();
    ExitButton.onClick.RemoveAllListeners();
    LetsBakeButton.onClick.RemoveAllListeners();
    TutorialBackButton.onClick.RemoveAllListeners();
    CookbookBackButton.onClick.RemoveAllListeners();
  }

}
