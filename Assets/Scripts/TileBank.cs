using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "TileBank", menuName = "Cake/TileBank")]
public class TileBank : ScriptableObject
{
  public GameObject TilePrefab;
  public Sprite Corner;
  public Sprite Horizontal;
  public Sprite Vertical;
  public List<TileBehaviour> TileBehaviours = new List<TileBehaviour>();

  private TileBehaviour[] SpawnableTileBehaviours;
  private List<TileType> SpawnableTileTypes = new List<TileType>();

  public int SpawnableTileTypeCount => SpawnableTileTypes.Count;

  public void Init()
  {
    int arraySize = Enum.GetValues(typeof(TileType)).Cast<int>().Max() + 1;
    SpawnableTileBehaviours = new TileBehaviour[arraySize];
    SpawnableTileTypes.Clear();

    foreach (TileBehaviour tileBehaviour in TileBehaviours)
    {
      if (!tileBehaviour) continue;
      SpawnableTileBehaviours[(int)tileBehaviour.TileType] = tileBehaviour;
    }

    foreach (TileBehaviour tileBehaviour in SpawnableTileBehaviours)
    {
      if (!tileBehaviour) continue;
      SpawnableTileTypes.Add(tileBehaviour.TileType);
    }
  }

  public TileType GetSpawnableTileType(int index)
  {
    return SpawnableTileTypes[index];
  }

  public TileBehaviour GetTileBehaviour(TileType tileType)
  {
    return Instantiate(SpawnableTileBehaviours[(int)tileType]);
  }

  public int GetTileScore(TileType tileType)
  {
    return SpawnableTileBehaviours[(int)tileType].TileData.TileScore;
  }

  public Sprite GetTileSprite(TileType tileType)
  {
    return SpawnableTileBehaviours[(int)tileType].TileData.TileSprite;
  }

  public string GetTileName(TileType tileType)
  {
    return SpawnableTileBehaviours[(int)tileType].TileData.TileName;
  }

  public TileData GetTileData(TileType tileType)
  {
    return SpawnableTileBehaviours[(int)tileType].TileData;
  }

}
