using System;
using UnityEngine;

[Serializable]
public struct TileData
{
  public string TileName;
  public int TileScore;

  public Sprite TileSprite;

  [TextArea]
  public string TileDescription;
}
