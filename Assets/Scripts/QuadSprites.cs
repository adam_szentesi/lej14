using UnityEngine;

public class QuadSprites : MonoBehaviour
{
  public SpriteRenderer UpperLeft;
  public SpriteRenderer UpperRight;
  public SpriteRenderer LowerLeft;
  public SpriteRenderer LowerRight;

}
