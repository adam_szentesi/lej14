using UnityEngine;

[CreateAssetMenu(fileName = "AcidTileBehaviour", menuName = "Cake/AcidTileBehaviour")]
public class AcidTileBehaviour : TileBehaviour
{
  public override void Toggle()
  {
    foreach (Vector2Int offset in NeighborOffsets)
    {
      Vector2Int targetPosition = TilePosition + offset;
      TileType targetTileType = TileGrid.Instance.GetTileType(targetPosition);

      if
      (
        targetTileType == TileType.Sponge
        || targetTileType == TileType.Weevil
        || targetTileType == TileType.Jam
        || targetTileType == TileType.Diamondium
      )
      {
        TileGrid.Instance.MarkForDestruction(targetPosition);
      }
    }
  }
}
