using UnityEngine;

[CreateAssetMenu(fileName = "WeevilTileBehaviour", menuName = "Cake/WeevilTileBehaviour")]
public class WeevilTileBehaviour : TileBehaviour
{
  public override void Toggle()
  {
    if (KillRivals())
    {
      TileGrid.Instance.MarkForDestruction(TilePosition);
      return;
    }

    EatDownwards();
  }

  private bool KillRivals()
  {
    bool result = false;

    foreach (Vector2Int offset in NeighborOffsets)
    {
      Vector2Int targetPosition = TilePosition + offset;
      TileType targetTileType = TileGrid.Instance.GetTileType(targetPosition);

      if (targetTileType == TileType.Weevil)
      {
        TileGrid.Instance.MarkForDestruction(targetPosition);
        result = true;
      }
    }

    return result;
  }

  private void EatDownwards()
  {
    Vector2Int targetPosition = TilePosition + new Vector2Int(0, -1);
    TileType targetTileType = TileGrid.Instance.GetTileType(targetPosition);

    if
    (
      targetTileType == TileType.Sponge
      || targetTileType == TileType.Jam
      || targetTileType == TileType.Bismol
    )
    {
      TileGrid.Instance.MarkForDestruction(targetPosition);
    }
  }

}
