using UnityEngine;

[CreateAssetMenu(fileName = "BismolTileBehaviour", menuName = "Cake/BismolTileBehaviour")]
public class BismolTileBehaviour : TileBehaviour
{
  public override void Toggle()
  {
    if (SeekAndDestroy(TilePosition, TileType.Acid))
    {
      SeekAndDestroy(TilePosition, TileType.Bismol);
      TileGrid.Instance.MarkForDestruction(TilePosition);
    }
  }

  private bool SeekAndDestroy(Vector2Int position, TileType tileType)
  {
    bool result = false;

    foreach (Vector2Int offset in NeighborOffsets)
    {
      Vector2Int targetPosition = position + offset;
      TileType targetTileType = TileGrid.Instance.GetTileType(targetPosition);

      if (targetTileType == tileType && TileGrid.Instance.MarkForDestruction(targetPosition))
      {
        SeekAndDestroy(targetPosition, tileType);
        result = true;
      }
    }

    return result;
  }

}
