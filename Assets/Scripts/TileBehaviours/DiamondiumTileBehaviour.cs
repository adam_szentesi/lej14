using UnityEngine;

[CreateAssetMenu(fileName = "DiamondiumTileBehaviour", menuName = "Cake/DiamondiumTileBehaviour")]
public class DiamondiumTileBehaviour : TileBehaviour
{
  public override void Toggle()
  {
    Vector2Int targetPosition = TilePosition + new Vector2Int(0, -1);
    TileType targetTileType = TileGrid.Instance.GetTileType(targetPosition);

    if
    (
      targetTileType == TileType.Empty
      || targetTileType == TileType.Error
      || targetTileType == TileType.Sponge
      || targetTileType == TileType.Diamondium
    )
    return;

    TileGrid.Instance.MarkForDestruction(targetPosition);
  }

}
