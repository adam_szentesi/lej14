using UnityEngine;

public class GameSetup : MonoBehaviour
{
  public int Width;
  public int Height;
  public float Slowness;

  public static GameSetup Instance;

  private void Awake()
  {
    //if (Instance)
    //{
    //  Destroy(gameObject);
    //  return;
    //}

    Instance = this;

    DontDestroyOnLoad(gameObject);
  }

}
