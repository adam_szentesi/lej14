using System;
using UnityEngine;

public class Tile : MonoBehaviour
{
  public SpriteRenderer TileSpriteRenderer;
  public QuadSprites BorderQuadSprites;

  public TileType TileType => TileBehaviour.TileType;
  
  public bool IsToBeDestroyed { get; private set; } = false;
  public bool IsControllable { get; private set; } = true;
  public Vector2Int TilePosition => TileBehaviour.TilePosition;

  private Action<Tile> OnDestroy;
  private TileBehaviour TileBehaviour;

  public void Init(TileBehaviour tileBehaviour, Action<Tile> onDestroy)
  {
    TileBehaviour = tileBehaviour;
    OnDestroy = onDestroy;

    TileSpriteRenderer.sprite = tileBehaviour.TileData.TileSprite;
  }

  public void SetTilePosition(Vector2Int tilePosition)
  {
    TileBehaviour.SetTilePosition(tilePosition);
    transform.position = new Vector3(TilePosition.x, TilePosition.y, 0);
  }

  public bool CanFall
  {
    get
    {
      Vector2Int targetPosition = TilePosition + new Vector2Int(0, -1);
      TileType targetTileType = TileGrid.Instance.GetTileType(targetPosition);

      return (targetTileType == TileType.Empty);
    }
  }

  public bool CanOverlap(TileType targetTileType)
  {
    return targetTileType == TileType.Empty;
  }

  public void DisableControls()
  {
    IsControllable = false;
  }

  public void Toggle()
  {
    if (IsControllable) return;
    TileBehaviour.Toggle();
  }

  public bool MarkForDestruction()
  {
    if (IsToBeDestroyed) return false;

    IsToBeDestroyed = true;
    return true;
  }

  public void Selfdestruct()
  {
    OnDestroy?.Invoke(this);
    Destroy(gameObject);
  }

  public void Reconnect()
  {
    TileType left = TileGrid.Instance.GetTileType(TilePosition + TileBehaviour.NeighborOffsets[0]);
    TileType right = TileGrid.Instance.GetTileType(TilePosition + TileBehaviour.NeighborOffsets[1]);
    TileType up = TileGrid.Instance.GetTileType(TilePosition + TileBehaviour.NeighborOffsets[3]);
    TileType down = TileGrid.Instance.GetTileType(TilePosition + TileBehaviour.NeighborOffsets[2]);

    bool leftSame = left == TileType;
    bool rightSame = right == TileType;
    bool upSame = up == TileType;
    bool downSame = down == TileType;

    CheckNeighbourhood(BorderQuadSprites.UpperLeft, leftSame, upSame);
    CheckNeighbourhood(BorderQuadSprites.UpperRight, rightSame, upSame);
    CheckNeighbourhood(BorderQuadSprites.LowerLeft, leftSame, downSame);
    CheckNeighbourhood(BorderQuadSprites.LowerRight, rightSame, downSame);
  }

  private void CheckNeighbourhood(SpriteRenderer renderer, bool horizontal, bool vertical)
  {
    if (!horizontal && !vertical)
    {
      renderer.sprite = TileGrid.Instance.TileBank.Corner;
    }
    else
    {
      if (horizontal && vertical)
      {
        renderer.sprite = null;
      }
      else
      {
        if (horizontal)
        {
          renderer.sprite = TileGrid.Instance.TileBank.Horizontal;
        }
        else
        {
          renderer.sprite = TileGrid.Instance.TileBank.Vertical;
        }
      }
    }
  }

}
