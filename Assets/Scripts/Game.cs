using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static UnityEngine.InputSystem.InputAction;

public class Game : MonoBehaviour
{
  public TileGrid TileGrid;
  public float WaitTime = 1.0f;

  public Text ScoreText;
  public Panel LeftPanel;
  public Panel RightPanel;
  public Panel BakeOverPanel;
  public Button PlayButton;
  public Button QuitButton;
  public Button OkButton;

  public Text PriceValueText;
  public Text LayerTipValueText;
  public Text TotalMoneyValueText;

  private InputActions InputActions;
  private int Score = 0;
  private bool IsPaused = false;

  private void Awake()
  {
    InputActions = new InputActions();

    InputActions.Play.Move.performed += Move;
    InputActions.Play.Lower.performed += Lower;
    InputActions.Play.Drop.performed += Drop;
    InputActions.Play.Pause.performed += TogglePause;
    InputActions.Play.Enable();

    WaitTime = 2.5f / Menu.Speed;
    TileGrid.Init(BakeOver, Menu.Width, Menu.Height);

    StartCoroutine(WaitCoroutine());
    
    LeftPanel.Toggle(false);
    BakeOverPanel.Toggle(false);

    PlayButton.onClick.AddListener(() => TogglePause());
    QuitButton.onClick.AddListener(Quit);
    OkButton.onClick.AddListener(() => SceneManager.LoadScene(0));
  }

  private IEnumerator WaitCoroutine()
  {
    TileGrid.SpawnTiles();

    yield return new WaitForSeconds(WaitTime);

    Score += TileGrid.GroundTiles();
    TileGrid.ToggleTiles();
    Score -= TileGrid.DestroyTiles();
    TileGrid.LowerTiles();
    TileGrid.ReconnectTiles();

    ScoreText.text = "�" + Score.ToString();

    StartCoroutine(WaitCoroutine());
  }

  private void Move(CallbackContext callbackContext)
  {
    int value = (int)callbackContext.ReadValue<float>();
    TileGrid.Move(value);
  }

  private void Lower(CallbackContext callbackContext)
  {
    TileGrid.Lower();
  }

  private void Drop(CallbackContext callbackContext)
  {
    Score += TileGrid.Drop();
  }

  private void TogglePause(CallbackContext callbackContext)
  {
    TogglePause();
  }

  private void TogglePause()
  {
    IsPaused = !IsPaused;
    Time.timeScale = IsPaused ? 0 : 1;
    LeftPanel.Toggle(IsPaused);
  }

  private void Quit()
  {
    TogglePause();
    SceneManager.LoadScene(0);
  }

  private void BakeOver()
  {
    LeftPanel.Toggle(false);
    RightPanel.Toggle(false);
    BakeOverPanel.Toggle(true);

    PriceValueText.text = "�" + Score.ToString();
    int layerScore = TileGrid.LayerScore;
    LayerTipValueText.text = "�" + layerScore.ToString();
    TotalMoneyValueText.text = "�" + (Score + layerScore).ToString();
  }

  private void OnDestroy()
  {
    PlayButton.onClick.RemoveAllListeners();
    QuitButton.onClick.RemoveAllListeners();
    OkButton.onClick.RemoveAllListeners();
    InputActions.Play.Disable();
  }

}
