using UnityEngine;

public class IntNumber : Number<int>
{
  protected override void Add()
  {
    Value += Increment;
  }

  protected override void Subtract()
  {
    Value -= Increment;
  }

  protected override int Validate(int value)
  {
    return Mathf.Clamp(value, Min, Max);
  }

}
