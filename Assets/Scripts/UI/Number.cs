using UnityEngine;
using UnityEngine.UI;

public abstract class Number<T> : MonoBehaviour
{
  public Button MinusButton;
  public Button PlusButton;
  public Text ValueText;

  private T _Value;
  public T Value
  {
    get
    {
      return _Value;
    }
    protected set
    {
      _Value = Validate(value);
      ValueText.text = _Value.ToString();
    }
  }

  protected T Min;
  protected T Max;
  protected T Increment;

  public void Init(T min, T max, T increment, T value)
  {
    Min = min;
    Max = max;
    Increment = increment;
    Value = value;

    PlusButton.onClick.AddListener(Add);
    MinusButton.onClick.AddListener(Subtract);
  }

  protected abstract void Add();
  protected abstract void Subtract();
  protected abstract T Validate(T value);

  private void OnDestroy()
  {
    PlusButton.onClick.RemoveAllListeners();
    MinusButton.onClick.RemoveAllListeners();
  }

}
