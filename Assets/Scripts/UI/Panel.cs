using UnityEngine;

public class Panel : MonoBehaviour
{
  public void Toggle(bool state)
  {
    gameObject.SetActive(state);
  }

}
