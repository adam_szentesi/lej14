using UnityEngine;

public class FloatNumber : Number<float>
{
  protected override void Add()
  {
    Value += Increment;
  }

  protected override void Subtract()
  {
    Value -= Increment;
  }

  protected override float Validate(float value)
  {
    return Mathf.Clamp(value, Min, Max);
  }

}
