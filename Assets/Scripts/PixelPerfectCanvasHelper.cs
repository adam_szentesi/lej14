using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas), typeof(CanvasScaler))]
public class PixelPerfectCanvasHelper : MonoBehaviour
{
  [SerializeField]
  private PixelPerfectCamera PixelPerfectCamera;
  private CanvasScaler CanvasScaler;

  private void Awake()
  {
    CanvasScaler = GetComponent<CanvasScaler>();
  }

  private void Update()
  {
    CanvasScaler.scaleFactor = PixelPerfectCamera ? PixelPerfectCamera.pixelRatio : 1;
  }

}
